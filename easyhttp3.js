/**
 * Easy HTTP Library
 * Library for making HTTP requests
 * 
 * @version 3.0.0
 * @author Sushil Meher
 * @license MIT
 * 
 */

 class EasyHTTP {
    // Make HTTP get request 
    async get(url) {
        const response = await fetch(url);
        const resData = await response.json();
        return resData;        
     }

     // Make HTTP post request
     async post(url, data) {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        const resData = await response.json();
        return resData;
     }

     // Make HTTP put request
     async put(url, data) {
        const response = await fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        const resData = await response.json();
        return resData;    
     }

     // Male HTTP delete request
     async delete(url) {
        const response = await fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const resData = await 'Resource Deleted!!!';
        return resData;
     }
 }